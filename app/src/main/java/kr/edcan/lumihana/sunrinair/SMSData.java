package kr.edcan.lumihana.sunrinair;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class SMSData {
    private String reqPhone;
    private String reqMessage;

    public SMSData(String reqPhone, String reqMessage) {
        this.reqPhone = reqPhone;
        this.reqMessage = reqMessage;
    }

    public String getReqPhone() {
        return reqPhone;
    }

    public String getReqMessage() {
        return reqMessage;
    }
}
