package kr.edcan.lumihana.sunrinair;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class ResponeThread extends Thread implements LocationListener {
    public static final int BATTERY_LEVEL = 0;
    public static final int BATTERY_STATUS = 1;
    public static final int BATTERY_VOLTAGE = 2;
    public static final int BATTERY_TEMPERATURE = 3;

    private static final String aPhone= "01030613553"; //긴급 문자 자동 전송 번호
    private boolean aAlarm = true;

    private SMSData data;
    private int position;
    private Context context;
    private PendingIntent sentIntent;
    private PendingIntent deliveredIntent;
    private String sendMessage = "";

    private Intent batteryState = null;

    private double latitude = -1.0;
    private double longitude = -1.0;
    LocationManager locationManager;
    String provider = "";

    public ResponeThread(Context context) {
        this.context = context;
    }

    private int[] getBatteryState() {
        int[] batteryStates;

        if(batteryState == null) batteryState = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        batteryStates = new int[]{
                (batteryState.getIntExtra(BatteryManager.EXTRA_LEVEL, -1))*100/batteryState.getIntExtra(BatteryManager.EXTRA_SCALE, 100),
                batteryState.getIntExtra(BatteryManager.EXTRA_STATUS, -1),
                batteryState.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1),
                batteryState.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1)/10
        };

        return batteryStates;
    }

    private String getAddress(double latitude, double longitude) {
        String address = null;
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> list = null;

        try {
            list = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (Exception e) {
            return null;
        }

        if (list == null) return null;

        if (list.size() > 0) {
            Address addr = list.get(0);
            address = addr.getCountryName() + " "
                    + addr.getPostalCode() + " "
                    + addr.getLocality() + " "
                    + addr.getThoroughfare() + " "
                    + addr.getFeatureName();
        }

        return address;
    }

    private void getLocate() {
        locationManager = null;
        provider = null;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) return;
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);

        if (provider == null || !locationManager.isProviderEnabled(provider)) {
            List<String> list = locationManager.getAllProviders();

            for (int i = 0; i < list.size(); i++) {
                String temp = list.get(i);

                if (locationManager.isProviderEnabled(temp)) {
                    provider = temp;
                    break;
                }
            }
        }

        Location location = locationManager.getLastKnownLocation(provider);
        if (location == null) return;
        else onLocationChanged(location);
    }

    private void setSms() {
        String sendMessage = "";
        sentIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT_ACTION"), 0);
        deliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED_ACTION"), 0);
        BroadcastReceiver sentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK: {
                        Log.e("SMS", "성공");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE: {
                        Log.e("SMS", "실패");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_NO_SERVICE: {
                        Log.e("SMS", "서비스지역이 아님");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_RADIO_OFF: {
                        Log.e("SMS", "무선이 꺼져있음");
                        break;
                    }

                    case SmsManager.RESULT_ERROR_NULL_PDU: {
                        Log.e("SMS", "PDU NULL");
                        break;
                    }
                }
            }
        };

        BroadcastReceiver deliverReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK: {
                        Log.e("SMS", "도착완료");
                        break;
                    }

                    case Activity.RESULT_CANCELED: {
                        Log.e("SMS", "도착실패");
                        break;
                    }
                }
            }
        };

        context.registerReceiver(sentReceiver, new IntentFilter("SMS_SENT_ACTION"));
        context.registerReceiver(deliverReceiver, new IntentFilter("SMS_DELIVERED_ACTION"));
    }

    private void sendSMS(String number) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, sendMessage, sentIntent, deliveredIntent);
        sendMessage = "";
    }

    @Override
    public void run() {
        int cnt = 0;
        setSms();

        while (true) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(cnt%10 == 0) { //10초마다 경고 보내기

                if (getBatteryState()[BATTERY_LEVEL] <= 20 && aAlarm) {
                    sendMessage += "[경고] 배터리 20%미만";
                    sendSMS(aPhone);
                }

                if (getBatteryState()[BATTERY_STATUS] == BatteryManager.BATTERY_STATUS_DISCHARGING && aAlarm) {
                    sendMessage += "[경고] 배터리 방전중";
                    sendSMS(aPhone);
                }
            }

            Log.e("Whiling", cnt++ + "");
            if (SMSList.getPosition() > -1) {
                Log.e("SMSList checked", "running...");
                position = SMSList.getPosition();
                data = SMSList.get(position);
                SMSList.remove(position);

                String phone = data.getReqPhone();
                String message = data.getReqMessage();
                Log.e("phone message", phone + " " + message);

                if (message.equals("배터리")) {
                    int[] batteryStates = getBatteryState();

                    Log.d("battery", getBatteryState() + "");
                    sendMessage += "잔량 : " + batteryStates[BATTERY_LEVEL] +
                            "%, 상태 : "+ batteryStates[BATTERY_STATUS] +
                            ", 온도 : "+batteryStates[BATTERY_TEMPERATURE] +
                            ", 전압 : " + batteryStates[BATTERY_VOLTAGE]+"mV";
                } else if (message.equals("위치")) {
                    getLocate();
                    String temp = getAddress(latitude, longitude);
                    if (temp != null)
                        sendMessage += "현재 위치 주소 : " + temp + "지도 : http://maps.google.com/maps?f=d&saddr=&daddr=" + temp;
                    else sendMessage = "위치 정보를 얻을 수 없음";
                }else if(message.equals("경보 끄기")){
                    aAlarm = false;
                    sendMessage += "경보 꺼짐";
                    sendSMS(aPhone);
                }else if(message.equals("경보 켜기")){
                    aAlarm = true;
                    sendMessage += "경보 켜짐";
                    sendSMS(aPhone);
                }

                if (!sendMessage.equals("")) {
                    sendSMS(phone);
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
