package kr.edcan.lumihana.sunrinair;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static final int ID_SERVICE_START = R.id.main_button_serviceStart;
    public static final int ID_SERVICE_END = R.id.main_button_serviceEnd;

    private Button main_button_serviceStart, main_button_serviceEnd;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent = new Intent(getApplicationContext(), SMSResponse.class);
        main_button_serviceStart = (Button) findViewById(ID_SERVICE_START);
        main_button_serviceEnd = (Button) findViewById(ID_SERVICE_END);
        main_button_serviceStart.setOnClickListener(this);
        main_button_serviceEnd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case ID_SERVICE_START : {
                startService(intent);
                break;
            }

            case ID_SERVICE_END : {
                stopService(intent);
                break;
            }
        }
    }
}
