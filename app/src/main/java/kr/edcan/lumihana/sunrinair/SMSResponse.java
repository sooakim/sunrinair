package kr.edcan.lumihana.sunrinair;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class SMSResponse extends Service {
    private ResponeThread thread;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        thread = new ResponeThread(getApplicationContext());
        thread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        thread = null;
    }
}
